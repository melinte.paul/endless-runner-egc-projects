#include "Laborator1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>

using namespace std;

// Order of function calling can be seen in "Source/Core/World.cpp::LoopUpdate()"
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/World.cpp

Laborator1::Laborator1()
{
	r = 0;
	g = 0;
	b = 0;
	index = 0;
}

Laborator1::~Laborator1()
{
}

void Laborator1::Init()
{
	// Load a mesh from file into GPU memory
	{
		Mesh* box = new Mesh("box");
		box->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[box->GetMeshID()] = box;

		Mesh* sphere = new Mesh("sphere");
		sphere->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[sphere->GetMeshID()] = sphere;

		Mesh* teapot = new Mesh("teapot");
		teapot->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "teapot.obj");
		meshes[teapot->GetMeshID()] = teapot;

		meshNames.push_back("box");
		meshNames.push_back("sphere");
		meshNames.push_back("teapot"); 
	}
}

void Laborator1::FrameStart()
{

}

void Laborator1::Update(float deltaTimeSeconds)
{
	glm::ivec2 resolution = window->props.resolution;

	// sets the clear color for the color buffer
	glClearColor(r,g,b, 1);

	// clears the color buffer (using the previously set color) and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);

	// render the object
	RenderMesh(meshes["box"], glm::vec3(1, 0.5f, 0), glm::vec3(0.5f));

	// render the object again but with different properties
	RenderMesh(meshes["box"], glm::vec3(-1, 0.5f, 0));

	RenderMesh(meshes[meshNames[index]], position);

	if (jumping) { //Schimbarea pozitiei in timpul sariturii
		position += velocity * deltaTimeSeconds;
		velocity -= glm::vec3(0, 0.3f, 0) * deltaTimeSeconds;
		if (velocity.g < -1.f)
		{
			jumping = false;
			velocity = glm::vec3(0);
		}
	}
}

void Laborator1::FrameEnd()
{
	DrawCoordinatSystem();
}

// Read the documentation of the following functions in: "Source/Core/Window/InputController.h" or
// https://github.com/UPB-Graphics/Framework-EGC/blob/master/Source/Core/Window/InputController.h

void Laborator1::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_W)) {
		position += glm::vec3(0, 0, 1.f) * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_S)) {
		position -= glm::vec3(0, 0, 1.f) * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_Q)) {
		position += glm::vec3(0, 1.f, 0) * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_E)) {
		position -= glm::vec3(0, 1.f, 0) * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_A)) {
		position += glm::vec3(1.f, 0, 0) * deltaTime;
	}
	if (window->KeyHold(GLFW_KEY_D)) {
		position -= glm::vec3(1.f, 0, 0) * deltaTime;
	}

};

void Laborator1::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_F) {
		r = rand() % 11 / 10.f;
		g = rand() % 11 / 10.f;
		b = rand() % 11 / 10.f;
	}
	if (key == GLFW_KEY_G) {
		index = (index + 1) % 3;
	}//inceputul sariturii
	if (key == GLFW_KEY_SPACE) {
		if (!jumping) {
			jumping = true;
			velocity = glm::vec3(0.3f, 1.f, 0);
		}
	}
};

void Laborator1::OnKeyRelease(int key, int mods)
{
	// add key release event
};

void Laborator1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
};

void Laborator1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
};

void Laborator1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
	// treat mouse scroll event
}

void Laborator1::OnWindowResize(int width, int height)
{
	// treat window resize event
}
