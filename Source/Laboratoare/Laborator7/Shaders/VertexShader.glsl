#version 330

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texture_coord;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

// Uniforms for light properties
uniform vec3 light_position;
uniform vec3 eye_position;
uniform float material_kd;
uniform float material_ks;
uniform int material_shininess;

uniform vec3 object_color;

// Output value to fragment shader
out vec3 color;

void main()
{
	vec3 world_position = (Model * vec4(v_position, 1.0)).xyz;
	vec3 N = normalize((Model * vec4(v_normal, 0.0)).xyz);
	vec3 L = normalize(light_position - world_position);
	vec3 R = reflect(-L,N);
	vec3 V = normalize(eye_position - world_position);

	float ambient_light = 0.25;

	float diffuse_light = dot(N,L);

	float specular_light = 0;

	if (diffuse_light > 0)
	{
		specular_light = pow(max(0, dot(R,V)),material_shininess);
	}

	float d = distance(world_position, light_position);
	float attenuation = 1f + 0.005f * d + 0.001f * d * d;

	color = ((ambient_light + max(0, diffuse_light)) * material_kd + 
			specular_light * material_ks) * object_color / attenuation;

	gl_Position = Projection * View * Model * vec4(v_position, 1.0);
}
