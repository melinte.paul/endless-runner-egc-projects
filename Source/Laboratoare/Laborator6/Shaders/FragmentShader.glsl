#version 330

in vec3 frag_normal;
in vec3 frag_color;

layout(location = 0) out vec4 out_color;

uniform float time;

void main()
{
	out_color = abs(sin(time)) * vec4(frag_normal,1);
}