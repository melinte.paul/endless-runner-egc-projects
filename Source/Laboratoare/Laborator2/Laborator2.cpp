#include "Laborator2.h"

#include <vector>
#include <iostream>
#include <cmath>

#include <Core/Engine.h>

using namespace std;

Laborator2::Laborator2()
{
}

Laborator2::~Laborator2()
{

}

void Laborator2::Init()
{
	polygonMode = GL_FILL;

	// Load a mesh from file into GPU memory
	{
		Mesh* mesh = new Mesh("box");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "box.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		vector<VertexFormat> vertices
		{
			VertexFormat(glm::vec3(1, -1,  -1), glm::vec3(0, 1, 1)),
			VertexFormat(glm::vec3(1,-1,1),glm::vec3(1,0,1)),
			VertexFormat(glm::vec3(-1,-1,0),glm::vec3(1,1,0)),
			VertexFormat(glm::vec3(0,1,0),glm::vec3(1,1,1))
		};

		vector<unsigned short> indices =
		{
			0,1,2,
			3,1,0,
			3,2,1,
			3,0,2
		};

		CreateMesh("tetraedru", vertices, indices);
	}

	{
		vector<VertexFormat> vertices
		{
			VertexFormat(glm::vec3(0, 1,  0), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(1, 1,  0), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(0, 1,  1), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(1, 1,  1), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(0, 0,  0), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(1, 0,  0), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(0, 0,  1), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(1, 0,  1), glm::vec3(0, 0, 1)),
			VertexFormat(glm::vec3(0, 1,  0), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(1, 1,  0), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(0, 1,  1), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(1, 1,  1), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(0, 0,  0), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(1, 0,  0), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(0, 0,  1), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(1, 0,  1), glm::vec3(0, 1, 0)),
			VertexFormat(glm::vec3(0, 1,  0), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(1, 1,  0), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(0, 1,  1), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(1, 1,  1), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(0, 0,  0), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(1, 0,  0), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(0, 0,  1), glm::vec3(1, 0, 0)),
			VertexFormat(glm::vec3(1, 0,  1), glm::vec3(1, 0, 0)),

		};

		vector<unsigned short> indices =
		{
			0,1,2,
			1,3,2,
			10,11,15,
			10,15,14,
			17,23,19,
			17,21,23,
			6,7,4,
			7,5,4,
			8,12,9,
			9,12,13,
			18,22,20,
			16,18,20
		};

		CreateMesh("cub", vertices, indices);
	}

	{
		vector<VertexFormat> vertices
		{
			VertexFormat(glm::vec3(0, 0, 1), glm::vec3(1, 1, 1)),
			VertexFormat(glm::vec3(1, 0, 1),glm::vec3(1,1,1)),
			VertexFormat(glm::vec3(0,1,1),glm::vec3(1,1,1)),
			VertexFormat(glm::vec3(1,1,1),glm::vec3(1,1,1))
		};

		vector<unsigned short> indices =
		{
			0,2,1,
			1,3,2,
		};

		CreateMesh("patrat", vertices, indices);
	}

	{
		float step_theta = 5.;
		float step_phi = 5.;

		vector<VertexFormat> vertices
		{
			VertexFormat(glm::vec3(0, 1, 0), glm::vec3(1)),
		};

		vector<unsigned short> indices =
		{};

		for (float phi = step_phi; phi < 180.; phi += step_phi) {
			for (float theta = 0.; theta < 360.; theta += step_theta) {
				vertices.push_back(VertexFormat(glm::vec3(cos(M_PI * theta / 180)* sin(M_PI* phi / 180), 
					cos(M_PI* phi / 180), sin(M_PI* theta / 180)* sin(M_PI* phi / 180)), glm::vec3(1)));
			}
		}
		unsigned short i;
		for (i = 1; i * step_theta < 360.; i++) {
			indices.push_back(i + 1);
			indices.push_back(i);
			indices.push_back(0);
		}

		indices.push_back(1);
		indices.push_back(i);
		indices.push_back(0);

		unsigned short ring_count = 360. / step_theta;
		unsigned short j;

		for (i = 1; (i+1) * step_phi < 180.; i++) {
			for (j = 0; (j+1) * step_theta < 360.; j++) {
				cerr << i << " " << j<<endl;
				indices.push_back(i* ring_count + j + 2);
				indices.push_back(i* ring_count + j + 1);
				indices.push_back((i - 1)* ring_count + j + 1);

				indices.push_back((i - 1)* ring_count + j + 1);
				indices.push_back((i - 1)* ring_count + j + 2);
				indices.push_back(i* ring_count + j + 2);
			}			
			indices.push_back(i* ring_count + 1);
			indices.push_back(i* ring_count + j + 1);
			indices.push_back((i - 1)* ring_count + j + 1);

			indices.push_back(i* ring_count + 1);
			indices.push_back((i - 1)* ring_count + j + 1);
			indices.push_back((i - 1)* ring_count + 1);
		}

		for (i = vertices.size() - ring_count; i < vertices.size() - 1; i++) {
			indices.push_back(i);
			indices.push_back(i + 1);
			indices.push_back(vertices.size());
		}

		indices.push_back(i);
		indices.push_back(vertices.size() - ring_count);
		indices.push_back(vertices.size());

		vertices.push_back(VertexFormat(glm::vec3(0, -1, 0), glm::vec3(1)));

		CreateMesh("sfera", vertices, indices);
	}

}

Mesh* Laborator2::CreateMesh(const char *name, const std::vector<VertexFormat> &vertices, const std::vector<unsigned short> &indices)
{
	unsigned int VAO = 0;
	// TODO: Create the VAO and bind it
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// TODO: Create the VBO and bind it
	unsigned int VBO = 0;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER,VBO);

	// TODO: Send vertices data into the VBO buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	// TODO: Crete the IBO and bind it
	unsigned int IBO = 0;
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

	// TODO: Send indices data into the IBO buffer
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	// ========================================================================
	// This section describes how the GPU Shader Vertex Shader program receives data
	// It will be learned later, when GLSL shaders will be introduced
	// For the moment just think that each property value from our vertex format needs to be send to a certain channel
	// in order to know how to receive it in the GLSL vertex shader

	// set vertex position attribute
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), 0);

	// set vertex normal attribute
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(sizeof(glm::vec3)));

	// set texture coordinate attribute
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3)));

	// set vertex color attribute
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(2 * sizeof(glm::vec3) + sizeof(glm::vec2)));
	// ========================================================================

	// TODO: Unbind the VAO
	glBindVertexArray(0);

	// Check for OpenGL errors
	CheckOpenGLError();

	// Mesh information is saved into a Mesh object
	meshes[name] = new Mesh(name);
	meshes[name]->InitFromBuffer(VAO, static_cast<unsigned short>(indices.size()));
	return meshes[name];
}

void Laborator2::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator2::Update(float deltaTimeSeconds)
{
	glLineWidth(3);
	glPointSize(5);
	glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

	// render an object using face normals for color
	RenderMesh(meshes["box"], shaders["VertexNormal"], glm::vec3(0, 0.5f, -1.5f), glm::vec3(0.75f));

	// render an object using colors from vertex
	RenderMesh(meshes["tetraedru"], shaders["VertexColor"], glm::vec3(-1.5f, 0.5f, 0), glm::vec3(0.25f));

	RenderMesh(meshes["cub"], shaders["VertexColor"], glm::vec3(1.5f, 0.5f, 0), glm::vec3(0.25f));

	RenderMesh(meshes["patrat"], shaders["VertexColor"], glm::vec3(2.f, 2.f, 0), glm::vec3(0.25f));

	RenderMesh(meshes["sfera"], shaders["VertexColor"], glm::vec3(-2.f, 2.f, -2.f), glm::vec3(0.5f));

}

void Laborator2::FrameEnd()
{
	DrawCoordinatSystem();
}

void Laborator2::OnInputUpdate(float deltaTime, int mods)
{

}

void Laborator2::OnKeyPress(int key, int mods)
{
	// TODO: switch between GL_FRONT and GL_BACK culling
	// Save the state in "cullFace" variable and apply it in the Update() method not here

	if (key == GLFW_KEY_SPACE)
	{
		switch (polygonMode)
		{
		case GL_POINT:
			polygonMode = GL_FILL;
			break;
		case GL_LINE:
			polygonMode = GL_POINT;
			break;
		default:
			polygonMode = GL_LINE;
			break;
		}
	}

	if (key == GLFW_KEY_F2)
	{
		switch (cullFace)
		{
		case 0:
			glEnable(GL_CULL_FACE);	
			glCullFace(GL_BACK);
			cullFace++;
			break;
		case 1:
			glCullFace(GL_FRONT);		
			cullFace++;
			break;
		case 2:			
			glDisable(GL_CULL_FACE);			
			cullFace = 0;
			break;
		}
	}

}

void Laborator2::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator2::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator2::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
}

void Laborator2::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator2::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator2::OnWindowResize(int width, int height)
{
}
