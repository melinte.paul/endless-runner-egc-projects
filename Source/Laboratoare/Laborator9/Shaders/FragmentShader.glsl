#version 330
 
uniform sampler2D texture_1;
uniform sampler2D texture_2;
uniform int useTexture2;
in vec2 texcoord;

layout(location = 0) out vec4 out_color;

void main()
{
	vec4 color1 = texture2D(texture_1,texcoord);
	vec4 color2 = texture2D(texture_2,texcoord);

	if(color1.a < 0.5)
		discard;

	if(useTexture2 == 1)
		out_color = vec4(mix(color1.rgb, color2.rgb, 0.5), 1);
	else out_color = vec4(color1.rgb, 1);

}