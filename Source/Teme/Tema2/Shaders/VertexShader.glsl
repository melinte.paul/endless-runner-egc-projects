#version 330

// TODO: get vertex attributes from each location
layout(location = 0) in vec3 v_position;
layout(location = 1) in vec3 v_normal;
layout(location = 2) in vec2 v_texture;
layout(location = 3) in vec3 v_color;

// Uniform properties
uniform mat4 Model;
uniform mat4 View;
uniform mat4 Projection;

uniform float time;

out vec3 frag_normal;
out vec3 frag_color;

void main()
{
	frag_normal = v_normal;
	frag_color = v_color;

	vec3 new_position = v_position;
	new_position.x = v_position.x + sin(v_position.y*5.0*time)*0.1;
	new_position.y = v_position.y + sin(v_position.z*5.0*time)*0.1;

	gl_Position = Projection * View * Model * vec4(new_position, 1.0);
}
