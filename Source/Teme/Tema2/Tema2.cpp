#include "Tema2.h"
#include "Objects/ObjectsGen.h"
#include <Component/Transform/Transform.h>
#include <algorithm>
#include <time.h>
#include <iostream>
#include <Teme\Tema1\Objects\Transform2D.h>

Tema2::Tema2()
{
}

Tema2::~Tema2()
{
}

void Tema2::Init()
{
	srand(time(NULL));

	{
		//Interface camera
		glm::ivec2 resolution = window->GetResolution();
		camera = GetSceneCamera();
		camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
		camera->SetPosition(glm::vec3(0, 0, 50));
		camera->SetRotation(glm::vec3(0, 0, 0));
		camera->Update();
		GetCameraInput()->SetActive(false);
	}

	{
		Shader* shader = new Shader("ShaderInterface");
		shader->AddShader("Source/Teme/Tema2/Shaders/VertexShaderInterface.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema2/Shaders/FragmentShaderInterface.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;

		shader = new Shader("ObjectsShader");
		shader->AddShader("Source/Teme/Tema2/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema2/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	{
		meshes["sphere"] = ObjectsGen::CreateSphereMesh(5., glm::vec3(0.5));
		meshes["sphereOutline"] = ObjectsGen::CreateSphereMesh(5., glm::vec3(0));
		sphere = new Sphere(meshes["sphere"], meshes["sphereOutline"], shaders["ObjectsShader"], 
			glm::vec3(0, 0.5, 0), glm::vec3(1), glm::vec3(0));

		meshes["cubeBlue"] = ObjectsGen::CreateCubeMesh(glm::vec3(0, 0, 0.5));
		meshes["cubePurple"] = ObjectsGen::CreateCubeMesh(glm::vec3(0.4, 0, 0.6));
		meshes["cubeRed"] = ObjectsGen::CreateCubeMesh(glm::vec3(0.7, 0, 0));
		meshes["cubeYellow"] = ObjectsGen::CreateCubeMesh(glm::vec3(1, 1, 0.2));
		meshes["cubeOrange"] = ObjectsGen::CreateCubeMesh(glm::vec3(1, 0.7, 0.2));
		meshes["cubeGreen"] = ObjectsGen::CreateCubeMesh(glm::vec3(0, 0.7, 0));

		meshes["fuelRectangleBlack"] = ObjectsGen::CreateRectangle2D("fuelRectangleBlack", 1, 1, glm::vec3(0));
		meshes["fuelRectangleGreen"] = ObjectsGen::CreateRectangle2D("fuelRectangleGreen", 1, 1 , glm::vec3(0, 0.8, 0));
		meshes["speedRectangleRed"] = ObjectsGen::CreateRectangle2D("speedRectangleRed", 1, 1, glm::vec3(0.8, 0, 0));
	}

	{
		leftPlatformsLength = 0;
		centerPlatformsLength = 6;
		rightPlatformsLength = 0;

		platforms = { new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(-2, -1, -1.5), glm::vec3(5, 1, 3)) };

		while (leftPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(leftPlatformsLength, -1, -6.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			leftPlatformsLength = leftPlatformsLength + newPlatformLength + newSpaceLength;
		}


		while (centerPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(centerPlatformsLength, -1, -1.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			centerPlatformsLength = centerPlatformsLength + newPlatformLength + newSpaceLength;
		}

		while (rightPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(rightPlatformsLength, -1, 3.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			rightPlatformsLength = rightPlatformsLength + newPlatformLength + newSpaceLength;
		}

		timeSinceRed = 0;
		timeSinceYellow = 0;
		timeSinceOrange = 0;
		timeSinceGreen = 0;
	}

	{
		tp_camera = new EngineComponents::Camera();
		tp_camera->SetPerspective(100, window->props.aspectRatio, 0.01f, 50);
		tp_camera->transform->SetMoveSpeed(0);
		tp_camera->transform->SetWorldPosition(glm::vec3(-3, 2.5f, 0));
		tp_camera->transform->SetWorldRotation(glm::vec3(-25, -90, 0));
		tp_camera->Update();

		fp_camera = new EngineComponents::Camera();
		fp_camera->SetPerspective(100, window->props.aspectRatio, 0.01f, 50);
		fp_camera->transform->SetMoveSpeed(0);
		fp_camera->transform->SetWorldPosition(glm::vec3(0.5, 0.5, 0));
		fp_camera->transform->SetWorldRotation(glm::vec3(0, -90, 0));
		fp_camera->Update();

		active_camera = tp_camera;
	}

	jumping = false;
	alive = true;
	speed = 0;
	fuel = 100;

	animationTime = 0.f;
}

void Tema2::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0.8, 0.8, 0.8, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Tema2::Update(float deltaTimeSeconds)
{
	if (deadNoFuelOrRed) {
		timeSincePlatDeath += deltaTimeSeconds;

		glm::vec3 newScale = sphere->getScale() - deltaTimeSeconds * glm::vec3(0.5);
		if (newScale.r < 0) {
			newScale = glm::vec3(0);
		}
		sphere->setScale(newScale);
	}

	if (deadFall) {
		timeSinceFall += deltaTimeSeconds;
		if (timeSinceFall <= 2.f) {
			active_camera = tp_camera;
			tp_camera->transform->SetWorldPosition(glm::vec3(-2, 1, 0) + sphere->getPos());
			tp_camera->transform->SetWorldRotation(glm::vec3(-25, -90, 0));
		}

		glm::vec3 newScale = sphere->getScale() - deltaTimeSeconds * glm::vec3(0.25);
		if (newScale.r < 0) {
			newScale = glm::vec3(0);
		}
		sphere->setScale(newScale);


	}

	{
		fp_camera->transform->SetWorldPosition(glm::vec3(0.5, 0.5, 0) + sphere->getPos());
		if (alive) tp_camera->MoveForward(deltaTimeSeconds);
		active_camera->Update();

		animationTime -= deltaTimeSeconds;
		animationTime = std::max(0.f, animationTime);

		//fuel -= 0.25 * speed * deltaTimeSeconds;
		fuel -= speed * deltaTimeSeconds;
		fuel = std::max(0.f, fuel);
		if (fuel <= 0) {
			alive = false;
			sphere->setSpeedX(0);

			deadNoFuelOrRed = true;
			timeSincePlatDeath = 0.f;
		}

		sphere->Update(deltaTimeSeconds);
		if (sphere->getPos().g == 0.5 && sphere->getSpeedY() == 0) {
			jumping = false;
		}
		if (sphere->getPos().g < -3) {
			alive = false;
			sphere->setSpeedX(0);

			deadFall = true;
			timeSinceFall = 0.f;
		}

		glLineWidth(3);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		RenderMesh(sphere->getOutline(), sphere->getShader(), sphere->getPos(), sphere->getScale(), sphere->getRotation());

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		RenderMesh(sphere->getMesh(), sphere->getShader(), sphere->getPos(), sphere->getScale(), sphere->getRotation());
	}

	{
		float sphereX = sphere->getPos().r;

		for (auto it = platforms.begin(); it != platforms.end(); it++) {
			Platform* platform = *it;
			if (platform->getPos().r + platform->getScale().r + 5 < sphereX) {
				it = platforms.erase(it);
			}
		}

		timeLeftOrange -= deltaTimeSeconds;
		if (timeLeftOrange < 0) {
			timeLeftOrange = 0;
		}

		timeSinceRed += deltaTimeSeconds;
		timeSinceYellow += deltaTimeSeconds;
		timeSinceOrange += deltaTimeSeconds;
		timeSinceGreen += deltaTimeSeconds;

		if (leftPlatformsLength - sphereX < 50 || centerPlatformsLength - sphereX < 50 || rightPlatformsLength - sphereX < 50) {

			Mesh* newPlatforMesh = meshes["cubeBlue"];
			float chance;
			chance = (rand() % 100) / 99.f;
			if (timeSinceRed > 2 && chance * timeSinceRed >= 7) {
				newPlatforMesh = meshes["cubeRed"];
				timeSinceRed = 0;
			}
			else if (timeSinceYellow > 1 && chance * timeSinceYellow >= 4) {
				newPlatforMesh = meshes["cubeYellow"];
				timeSinceYellow = 0;
			}
			else if (timeSinceOrange > 2 && chance * timeSinceOrange >= 6) {
				newPlatforMesh = meshes["cubeOrange"];
				timeSinceOrange = 0;
			}
			else if (timeSinceGreen > 1 && chance * timeSinceGreen >= 5) {
				newPlatforMesh = meshes["cubeGreen"];
				timeSinceGreen = 0;
			}

			if (leftPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(leftPlatformsLength, -1, -6.5), glm::vec3(newPlatformLength, 1, 3)));
				int newSpaceLength = rand() % 10;
				leftPlatformsLength = leftPlatformsLength + newPlatformLength + newSpaceLength;
			}

			if (centerPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(centerPlatformsLength, -1, -1.5), glm::vec3(newPlatformLength, 1, 3)));
				int newSpaceLength = rand() % 10;
				centerPlatformsLength = centerPlatformsLength + newPlatformLength + newSpaceLength;

			}

			if (rightPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(rightPlatformsLength, -1, 3.5), glm::vec3(newPlatformLength, 1, 3)));
				int newSpaceLength = rand() % 10;
				rightPlatformsLength = rightPlatformsLength + newPlatformLength + newSpaceLength;

			}
		}

		for (auto it = platforms.begin(); it != platforms.end(); it++) {
			Platform* platform = *it;
			if (platform->checkAndHandleColision(sphere)) {
				if (platform->getMesh() == meshes["cubeRed"]) {
					alive = false;
					sphere->setSpeedX(0);

					deadNoFuelOrRed = true;
					timeSincePlatDeath = 0.f;

				}

				if (platform->getMesh() == meshes["cubeYellow"]) {
					fuel -= 25;
					fuel = std::max(0.f, fuel);

					animationTime = 2.f;
				}

				if (platform->getMesh() == meshes["cubeOrange"]) {
					speed = 5;
					timeLeftOrange = 5;
					tp_camera->transform->SetMoveSpeed(speed * 2);
					sphere->setSpeedX(speed * 2);

					animationTime = 2.f;
				}

				if (platform->getMesh() == meshes["cubeGreen"]) {
					fuel += 25;
					fuel = std::min(100.f, fuel);

					animationTime = 2.f;
				}

				platform->setMesh(meshes["cubePurple"]);
			}
			RenderMesh(platform->getMesh(), platform->getShader(), platform->getPos(), platform->getScale(), glm::vec3(0));
		}
	}

	
	{
	RenderMeshInterface(meshes["speedRectangleRed"], shaders["ShaderInterface"], glm::vec3(400 - 150 * ((5 - speed) / 5), 250, 0), glm::vec3(300 * (speed / 5), 75, 0), glm::vec3(0));
	RenderMeshInterface(meshes["fuelRectangleGreen"], shaders["ShaderInterface"], glm::vec3(400 - 150 * ((100.f - fuel) / 100.f), 150, 0), glm::vec3(300 * (fuel / 100.f), 75, 0), glm::vec3(0));
	RenderMeshInterface(meshes["fuelRectangleBlack"], shaders["ShaderInterface"], glm::vec3(400, 150, 0), glm::vec3(300, 75, 0), glm::vec3(0));
	}
}

void Tema2::RenderMeshInterface(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(camera->GetProjectionMatrix()));

	glm::mat4 model(1);
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	model = glm::rotate(model, rotation.r, glm::vec3(1, 0, 0));
	model = glm::rotate(model, rotation.g, glm::vec3(0, 1, 0));
	model = glm::rotate(model, rotation.b, glm::vec3(0, 0, 1));

	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(model));
	mesh->Render();
}

void Tema2::RenderMesh(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetProjectionMatrix()));

	int timeLocation = glGetUniformLocation(shader->program, "time");
	glUniform1f(timeLocation, animationTime);

	glm::mat4 model(1);
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	model = glm::rotate(model, rotation.r, glm::vec3(1, 0, 0));
	model = glm::rotate(model, rotation.g, glm::vec3(0, 1, 0));
	model = glm::rotate(model, rotation.b, glm::vec3(0, 0, 1));

	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(model));
	mesh->Render();
}


void Tema2::FrameEnd()
{
	//DrawCoordinatSystem(active_camera->GetViewMatrix(), active_camera->GetProjectionMatrix());
}


void Tema2::OnInputUpdate(float deltaTime, int mods)
{
	{
		if (window->KeyHold(GLFW_KEY_A) && alive) {
			sphere->moveLeft(deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_D) && alive) {
			sphere->moveRight(deltaTime);
		}
	}

}

void Tema2::OnKeyPress(int key, int mods)
{
	{
		if (key == GLFW_KEY_W && alive && timeLeftOrange == 0) {
			speed += 1;
		}
		if (key == GLFW_KEY_S && alive && timeLeftOrange == 0) {
			speed -= 1;
		}

		speed = std::min(speed, 5.f);
		speed = std::max(1.f, speed);

		tp_camera->transform->SetMoveSpeed(speed * 2);
		sphere->setSpeedX(speed * 2);
	}

	{
		if (key == GLFW_KEY_SPACE && jumping == false && alive) {
			sphere->setSpeedY(2);
			jumping = true;
		}
	}

	{
		if (key == GLFW_KEY_E && alive) {
			if (active_camera == tp_camera) {
				active_camera = fp_camera;
			}
			else active_camera = tp_camera;
		}
	}
}

void Tema2::OnKeyRelease(int key, int mods)
{
}

void Tema2::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
}

void Tema2::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
}

void Tema2::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
}

void Tema2::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Tema2::OnWindowResize(int width, int height)
{
}
