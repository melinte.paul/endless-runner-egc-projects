#pragma once
#include <Component\SimpleScene.h>

class Sphere {
	public:
		Sphere();
		Sphere(Mesh* mesh, Mesh* outline, Shader* shader,glm::vec3 pos, glm::vec3 scale, glm::vec3 rotation);
		~Sphere();

		void Update(float deltaTime);
		Mesh* getMesh();
		Mesh* getOutline();
		Shader* getShader();
		glm::vec3 getPos();
		glm::vec3 getScale();
		glm::vec3 getRotation();

		void setScale(glm::vec3 scale);
		void setSpeedX(float speedX);
		void moveLeft(float deltaTime);
		void moveRight(float deltaTime);
		void setSpeedY(float speedY);
		float getSpeedY();

		void setSolid(bool solid);
	private:
		Mesh* mesh;
		Mesh* outline;
		Shader* shader;

		glm::vec3 pos;
		glm::vec3 scale;
		glm::vec3 rotation;

		float speedX;
		float speedZ;
		float speedY;

		bool solid;
};