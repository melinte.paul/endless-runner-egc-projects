#include "Sphere.h"

Sphere::Sphere()
{
}

Sphere::Sphere(Mesh* mesh, Mesh* outline, Shader* shader, glm::vec3 pos, glm::vec3 scale, glm::vec3 rotation)
{
	this->mesh = mesh;
	this->outline = outline;
	this->shader = shader;
	this->pos = pos;
	this->scale = scale;
	this->rotation = rotation;

	speedX = 0;
	speedY = 0;
	speedZ = 2;
}

Sphere::~Sphere()
{
}

void Sphere::Update(float deltaTime)
{
	pos += speedX * deltaTime * glm::vec3(1, 0, 0);
	rotation += speedX * deltaTime * glm::vec3(0,0,-1);

	if (solid && speedY <= 0) {
		speedY = 0;
		pos.g = 0.5;
	}
	else {
		pos.g += speedY * deltaTime;
		speedY -= 1 * deltaTime;
	}
	solid = false;
}

Mesh* Sphere::getMesh()
{
	return mesh;
}

Mesh* Sphere::getOutline()
{
	return outline;
}

Shader* Sphere::getShader()
{
	return shader;
}

glm::vec3 Sphere::getPos()
{
	return pos;
}

glm::vec3 Sphere::getScale()
{
	return scale;
}

glm::vec3 Sphere::getRotation()
{
	return rotation;
}

void Sphere::setScale(glm::vec3 scale)
{
	this->scale = scale;
}

void Sphere::setSpeedX(float speedX)
{
	this->speedX = speedX;
}

void Sphere::moveLeft(float deltaTime)
{
	pos.b -= deltaTime * speedZ;
}

void Sphere::moveRight(float deltaTime)
{
	pos.b += deltaTime * speedZ;
}

void Sphere::setSpeedY(float speedY)
{
	this->speedY = speedY;
}

float Sphere::getSpeedY()
{
	return speedY;
}

void Sphere::setSolid(bool solid)
{
	this->solid = solid;
}
