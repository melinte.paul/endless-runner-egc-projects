
#include <Core/Engine.h>

namespace ObjectsGen {

	Mesh* CreateSphereMesh(float angleStep,glm::vec3 color) {
		float step_theta = 30.;
		float step_phi = 30.;

		std::vector<VertexFormat> vertices
		{
			VertexFormat(0.5f * glm::vec3(0, 1, 0), color),
		};

		std::vector<unsigned short> indices =
		{};

		for (float phi = step_phi; phi < 180.; phi += step_phi) {
			for (float theta = 0.; theta < 360.; theta += step_theta) {
				vertices.push_back(VertexFormat(0.5f * glm::vec3(cos(M_PI * theta / 180) * sin(M_PI * phi / 180),
					cos(M_PI * phi / 180), sin(M_PI * theta / 180) * sin(M_PI * phi / 180)), color));
			}
		}
		unsigned short i;
		for (i = 1; i * step_theta < 360.; i++) {
			indices.push_back(i + 1);
			indices.push_back(i);
			indices.push_back(0);
		}

		indices.push_back(1);
		indices.push_back(i);
		indices.push_back(0);

		unsigned short ring_count = 360. / step_theta;
		unsigned short j;

		for (i = 1; (i + 1) * step_phi < 180.; i++) {
			for (j = 0; (j + 1) * step_theta < 360.; j++) {
				indices.push_back(i * ring_count + j + 2);
				indices.push_back(i * ring_count + j + 1);
				indices.push_back((i - 1) * ring_count + j + 1);

				indices.push_back((i - 1) * ring_count + j + 1);
				indices.push_back((i - 1) * ring_count + j + 2);
				indices.push_back(i * ring_count + j + 2);
			}
			indices.push_back(i * ring_count + 1);
			indices.push_back(i * ring_count + j + 1);
			indices.push_back((i - 1) * ring_count + j + 1);

			indices.push_back(i * ring_count + 1);
			indices.push_back((i - 1) * ring_count + j + 1);
			indices.push_back((i - 1) * ring_count + 1);
		}

		for (i = vertices.size() - ring_count; i < vertices.size() - 1; i++) {
			indices.push_back(i);
			indices.push_back(i + 1);
			indices.push_back(vertices.size());
		}

		indices.push_back(i);
		indices.push_back(vertices.size() - ring_count);
		indices.push_back(vertices.size());

		vertices.push_back(VertexFormat(0.5f * glm::vec3(0, -1, 0), color));

		Mesh* sphere = new Mesh("sphere");
		sphere->SetDrawMode(GL_TRIANGLES);
		sphere->InitFromData(vertices, indices);

		return sphere;
	}

	Mesh* CreateCubeMesh(glm::vec3 color) {
		
		std::vector<VertexFormat> vertices
		{
			VertexFormat(glm::vec3(0, 1,  0), color),
			VertexFormat(glm::vec3(1, 1,  0), color),
			VertexFormat(glm::vec3(0, 1,  1), color),
			VertexFormat(glm::vec3(1, 1,  1), color),
			VertexFormat(glm::vec3(0, 0,  0), color),
			VertexFormat(glm::vec3(1, 0,  0), color),
			VertexFormat(glm::vec3(0, 0,  1), color),
			VertexFormat(glm::vec3(1, 0,  1), color),
			VertexFormat(glm::vec3(0, 1,  0), color),
			VertexFormat(glm::vec3(1, 1,  0), color),
			VertexFormat(glm::vec3(0, 1,  1), color),
			VertexFormat(glm::vec3(1, 1,  1), color),
			VertexFormat(glm::vec3(0, 0,  0), color),
			VertexFormat(glm::vec3(1, 0,  0), color),
			VertexFormat(glm::vec3(0, 0,  1), color),
			VertexFormat(glm::vec3(1, 0,  1), color),
			VertexFormat(glm::vec3(0, 1,  0), color),
			VertexFormat(glm::vec3(1, 1,  0), color),
			VertexFormat(glm::vec3(0, 1,  1), color),
			VertexFormat(glm::vec3(1, 1,  1), color),
			VertexFormat(glm::vec3(0, 0,  0), color),
			VertexFormat(glm::vec3(1, 0,  0), color),
			VertexFormat(glm::vec3(0, 0,  1), color),
			VertexFormat(glm::vec3(1, 0,  1), color),

		};

		std::vector<unsigned short> indices =
		{
			0,1,2,
			1,3,2,
			10,11,15,
			10,15,14,
			17,23,19,
			17,21,23,
			6,7,4,
			7,5,4,
			8,12,9,
			9,12,13,
			18,22,20,
			16,18,20
		};

		Mesh* cube = new Mesh("cube");
		cube->SetDrawMode(GL_TRIANGLES);
		cube->InitFromData(vertices, indices);

		return cube;
	}

	Mesh* CreateRectangle2D(std::string name, float length, float height, glm::vec3 color)
	{
		std::vector<VertexFormat> vertices =
		{
			VertexFormat(glm::vec3(-length / 2,-height / 2,0), color),
			VertexFormat(glm::vec3(-length / 2,height / 2,0), color),
			VertexFormat(glm::vec3(length / 2,-height / 2,0), color),
			VertexFormat(glm::vec3(length / 2,height / 2,0), color)
		};
		std::vector<unsigned short> indices = { 0, 1, 2, 3, 1, 2 };

		Mesh* rectangle = new Mesh(name);
		rectangle->InitFromData(vertices, indices);
		return rectangle;
	}

}