#pragma once
#include <Component\SimpleScene.h>
#include <Teme\Tema2\Objects\Sphere.h>

class Platform {
public:
	Platform();
	Platform(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale);
	~Platform();

	Mesh* getMesh();
	void setMesh(Mesh* mesh);
	Shader* getShader();
	glm::vec3 getPos();
	glm::vec3 getScale();

	bool checkAndHandleColision(Sphere* sphere);

private:
	Mesh* mesh;
	Shader* shader;
	glm::vec3 pos;
	glm::vec3 scale;
	bool touched;
};