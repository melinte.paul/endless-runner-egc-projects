#include "Platform.h"
#include <iostream>

Platform::Platform()
{
}

Platform::Platform(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale)
{
	this->mesh = mesh;
	this->shader = shader;
	this->pos = pos;
	this->scale = scale;
	touched = false;
}

Platform::~Platform()
{
}

Mesh* Platform::getMesh()
{
	return mesh;
}

void Platform::setMesh(Mesh* mesh)
{
	this->mesh = mesh;
}

Shader* Platform::getShader()
{
	return shader;
}

glm::vec3 Platform::getPos()
{
	return pos;
}

glm::vec3 Platform::getScale()
{
	return scale;
}

bool Platform::checkAndHandleColision(Sphere* sphere)
{
	if (sphere->getPos().g > 0.55 || sphere->getPos().g < 0.25 || sphere->getSpeedY() > 0) {

		return false;
	}

	if (pos.r < sphere->getPos().r && pos.r + scale.r > sphere->getPos().r
		&& pos.b < sphere->getPos().b && pos.b + scale.b > sphere->getPos().b) {
		sphere->setSolid(true);
		if (!touched) {
			touched = true;
			return true;
		}
	}

	return false;
}
