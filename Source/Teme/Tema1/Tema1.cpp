#include "Tema1.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>

#include "Objects/ObjectsGen.h"
#include "Objects/Transform2D.h"
#include <time.h>

Tema1::Tema1()
{
}

Tema1::~Tema1()
{
}

void Tema1::Init()
{
	glm::ivec2 resolution = window->GetResolution();
	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);
	glLineWidth(3);

	mouseY = 300;
	mouseX = 500;
	is_mouse_pressed = false;

	score = 0;
	lives = 3;
	alive = true;

	srand(time(NULL));

	yellowBallonTime = 0;
	yellowBallonTimeLimit = 3.;
	redBallonTime = 0;
	redBallonTimeLimit = 2.;

	{
		balloonRed = ObjectsGen::CreateBalloon("balloon", 100, 0.70, glm::vec3(1,0,0));
		balloonYellow = ObjectsGen::CreateBalloon("balloon", 100, 0.70, glm::vec3(1, 1, 0));
		balloonString = ObjectsGen::CreateBString("balloon_string", 50, 6, 15, glm::vec3(1));
		
		Mesh* explosion1 = ObjectsGen::CreateBalloonExplosion("explosion1", 10, 75, 7, glm::vec3(1, 0, 0));
		Mesh* explosion2 = ObjectsGen::CreateBalloonExplosion("explosion2", 20, 75, 7, glm::vec3(1, 0, 0));
		Mesh* explosion3 = ObjectsGen::CreateBalloonExplosion("explosion3", 20, 100, 7, glm::vec3(1, 0, 0));
		Mesh* explosion4 = ObjectsGen::CreateBalloonExplosion("explosion4", 30, 100, 7, glm::vec3(1, 0, 0));
		Mesh* explosion5 = ObjectsGen::CreateBalloonExplosion("explosion5", 100, 100, 7, glm::vec3(1, 0, 0));

		explosionSeqRed = { explosion1 ,explosion2 ,explosion3 ,explosion4 ,explosion5 };

		explosion1 = ObjectsGen::CreateBalloonExplosion("explosion1", 10, 75, 7, glm::vec3(1, 1, 0));
		explosion2 = ObjectsGen::CreateBalloonExplosion("explosion2", 20, 75, 7, glm::vec3(1, 1, 0));
		explosion3 = ObjectsGen::CreateBalloonExplosion("explosion3", 20, 100, 7, glm::vec3(1, 1, 0));
		explosion4 = ObjectsGen::CreateBalloonExplosion("explosion4", 30, 100, 7, glm::vec3(1, 1, 0));
		explosion5 = ObjectsGen::CreateBalloonExplosion("explosion5", 100, 100, 7, glm::vec3(1, 1, 0));

		explosionSeqYellow = { explosion1 ,explosion2 ,explosion3 ,explosion4 ,explosion5 };
	}

	shurikenTime = 0;
	shurikenTimeLimit = 1.5;

	{
		shuriken = ObjectsGen::CreateShuriken("shuriken", 50, glm::vec3(1));
	}

	{
		Mesh* bowMesh = ObjectsGen::CreateBow("bow", 100, glm::vec3(1));
		bow = *(new Bow(bowMesh, 200, 750, 0, 100));

		Mesh* arrowMesh = ObjectsGen::CreateArrow("arrow", 150, glm::vec3(1));
		arrow = *(new Arrow(arrowMesh, 350, 750, 0, 150));
		arrow_flying = false;

		Mesh* rectangleMesh = ObjectsGen::CreateRectangle("power_rectangle", 100, 10, glm::vec3(0, 1, 0));
		AddMeshToList(rectangleMesh);
	}
}

void Tema1::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0.75f, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Tema1::Update(float deltaTimeSeconds)
{
	if (lives <= 0)
		alive = false;

	{
		{
			float relative_x = mouseX - bow.getX();
			float relative_y = window->GetResolution().y - mouseY - bow.getY();

			float sin_xy = relative_y / (sqrt(relative_y * relative_y + relative_x * relative_x));

			if (relative_x < 0)
				if (relative_y < 0) sin_xy = -1;
				else sin_xy = 1;

			float angle = asin(sin_xy);

			bow.setAngle(angle);
			if (!arrow_flying) arrow.setAngle(angle);
		}

		if (is_mouse_pressed) power = std::min(1., power + .5 * deltaTimeSeconds);

		RenderMesh2D(bow.getMesh(), shaders["VertexColor"], bow.getModelMatrix());

		if (alive) arrow.update(deltaTimeSeconds);
		if (arrow.oob(window)) 
		{ 
			arrow.reset(bow); 
			arrow_flying = false;
		}
		RenderMesh2D(arrow.getMesh(), shaders["VertexColor"], arrow.getModelMatrix());

		RenderMesh2D(meshes["power_rectangle"], shaders["VertexColor"],
			glm::mat3(1) * Transform2D::Translate(bow.getX() + 50, bow.getY() - 120)
			* Transform2D::Scale(power, 1));
	}


	{
		{
			yellowBallonTime += deltaTimeSeconds;
			redBallonTime += deltaTimeSeconds;

			if (yellowBallonTime > yellowBallonTimeLimit) {
				yellowBallonTime = 0;
				yellowBallonTimeLimit *= 0.98;

				int pos = rand() % 2500 + 500;
				balloons.push_back(new Balloon(balloonYellow, balloonString, explosionSeqYellow, pos, -100, 100, -1));
			}

			if (redBallonTime > redBallonTimeLimit) {
				redBallonTime = 0;
				redBallonTimeLimit *= 0.99;

				int pos = rand() % 2500 + 500;
				balloons.push_back(new Balloon(balloonRed, balloonString, explosionSeqRed, pos, -100, 100, 1));
			}
		}
		for (auto it = balloons.begin(); it != balloons.end(); it++) {
			(*it)->set_hit(balloonArrowCol((*it)));

			if ((*it)->oob(window)) it = balloons.erase(it);
			else {
				if (alive) (*it)->update(deltaTimeSeconds);
				RenderMesh2D((*it)->getMeshBalloon(), shaders["VertexColor"], (*it)->getModelMatrixBalloon());
				RenderMesh2D((*it)->getMeshString(), shaders["VertexColor"], (*it)->getModelMatrixString());
			}
			if (it == balloons.end()) break;
		}
	}

	{
		{
			shurikenTime += deltaTimeSeconds;

			if (shurikenTime > shurikenTimeLimit) {
				shurikenTime = 0;
				shurikenTimeLimit *= 0.99;
				int pos = rand() % 1500;		

				shurikens.push_back(new Shuriken(shuriken, 3100, pos, 50));
			}
		}
		for (auto it = shurikens.begin(); it != shurikens.end(); it++) {
			(*it)->set_hit(shurikenArrowCol((*it)));
			(*it)->set_hit(shurikenBowCol((*it)));

			if ((*it)->oob(window)) it = shurikens.erase(it);
			else {
				if (alive) (*it)->update(deltaTimeSeconds);
				RenderMesh2D((*it)->getMesh(), shaders["VertexColor"], (*it)->getModelMatrix());
			}
			if (it == shurikens.end()) break;
		}
	}
}

bool Tema1::balloonArrowCol(Balloon* balloon)
{
	float distance = sqrt(pow(balloon->getX() - arrow.getX(), 2) + pow(balloon->getY() - arrow.getY(), 2));

	if (distance < 1.25 * balloon->getLen() && arrow_flying) { 
		if (!balloon->getIsHit()) {
			score += balloon->getScore();
			std::cout << "Scor:" << score << " Vieti:" << lives << std::endl;
		}
		return true; 
	}
	return false;
}

bool Tema1::shurikenArrowCol(Shuriken* shuriken)
{
	float distance = sqrt(pow(shuriken->getX() - arrow.getX(), 2) + pow(shuriken->getY() - arrow.getY(), 2));

	if (distance < 1.25 * shuriken->getLen() && arrow_flying) {
		if (!shuriken->getIsHit()) {
			score += 1;
			std::cout << "Scor:" << score << " Vieti:" << lives << std::endl;
		}
		return true;
	}
	return false;
}

bool Tema1::shurikenBowCol(Shuriken* shuriken)
{
	float distance = sqrt(pow(shuriken->getX() - bow.getX(), 2) + pow(shuriken->getY() - bow.getY(), 2));

	float bowAngle = bow.getAngle();
	float maxY = bow.getY() + bow.getLen();
	float minY = bow.getY() - bow.getLen();
	if (bowAngle > 0) {
		minY = bow.getY() - bow.getLen() * cos(bowAngle);
	}
	if (bowAngle < 0) {
		maxY = bow.getY() + bow.getLen() * cos(bowAngle);
	}

	if (shuriken->getY() - shuriken->getLen() > maxY ) {
		return false;
	}

	if (shuriken->getY() + shuriken->getLen() < minY) {
		return false;
	}

	if (distance < shuriken->getLen() + bow.getLen()) {
		if (!shuriken->getIsHit()) {
			lives -= 1;
			std::cout << "Scor:" << score << " Vieti:" << lives << std::endl;
		}
		return true;
	}
	return false;
}

void Tema1::FrameEnd()
{

}

void Tema1::OnInputUpdate(float deltaTime, int mods)
{
	if (alive)
		if (window->KeyHold(GLFW_KEY_W)) {
			float movement = 400. * deltaTime;
			bow.moveY(movement);
			if(!arrow_flying) arrow.moveY(movement);
		}
		if (window->KeyHold(GLFW_KEY_S)) {
			float movement = -400. * deltaTime;
			bow.moveY(movement);
			if (!arrow_flying) arrow.moveY(movement);
		}

}

void Tema1::OnKeyPress(int key, int mods)
{
	// add key press event
}

void Tema1::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Tema1::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	if (alive) {
		this->mouseX = mouseX;
		this->mouseY = mouseY;
	}
}

void Tema1::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	if (button == 1 && !arrow_flying && alive) {
		is_mouse_pressed = true;
		power = 0.;
	}
}

void Tema1::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	if (button == 1 && !arrow_flying && alive) {
		is_mouse_pressed = false;
		
		arrow.fly(power);
		arrow_flying = true;

		power = 0.;
	}
}

void Tema1::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Tema1::OnWindowResize(int width, int height)
{
}

