#include "Balloon.h"
#include <Teme\Tema1\Objects\Transform2D.h>
#include <iostream>

Balloon::Balloon()
{
}

Balloon::Balloon(Mesh * balloonMesh, Mesh* stringMesh, std::vector<Mesh*> explosionSeq, float x, float y, float length, int score_change)
{
	this->balloonMesh = balloonMesh;
	this->stringMesh = stringMesh;
	this->explosionSeq = explosionSeq;
	this->x = x;
	this->y = y;
	this->length = length;
	this->score_change = score_change;

	is_hit = false;
	time_since_hit = 0;
}

Balloon::~Balloon()
{
}

glm::mat3 Balloon::getModelMatrixBalloon()
{
	return glm::mat3(1) * Transform2D::Translate(x, y);
}

glm::mat3 Balloon::getModelMatrixString()
{
	return glm::mat3(1) * Transform2D::Translate(x, y - length - time_since_hit * 300.);
}

Mesh* Balloon::getMeshBalloon()
{
	return balloonMesh;
}

Mesh* Balloon::getMeshString()
{
	return stringMesh;
}

void Balloon::update(float deltaTime)
{
	if (is_hit) {
		time_since_hit += deltaTime;
		y - +50 * deltaTime;
		if (time_since_hit < .25) balloonMesh = explosionSeq.at(0);
		else if (time_since_hit < .5) balloonMesh = explosionSeq.at(1);
		else if (time_since_hit < .75) balloonMesh = explosionSeq.at(2);
		else if (time_since_hit < 1) balloonMesh = explosionSeq.at(3);
		else if (time_since_hit < 1.5) balloonMesh = explosionSeq.at(4);
	}
	else y += 150. * deltaTime;
}

bool Balloon::oob(WindowObject* window)
{
	if (is_hit) {
		return y - length - time_since_hit * 300. < 0;
	}
	return window->GetResolution().y + 2*length < y;
}

float Balloon::getX()
{
	return x;
}

float Balloon::getY()
{
	return y;
}

float Balloon::getLen()
{
	return length;
}

void Balloon::set_hit(bool is_hit)
{
	if(!this->is_hit) this->is_hit = is_hit;
}

int Balloon::getScore()
{
	return score_change;
}

bool Balloon::getIsHit()
{
	return is_hit;
}
