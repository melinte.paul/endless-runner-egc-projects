#include "Shuriken.h"
#include <Teme\Tema1\Objects\Transform2D.h>

Shuriken::Shuriken()
{
}

Shuriken::Shuriken(Mesh* mesh, float x, float y, float length)
{
	this->mesh = mesh;
	this->x = x;
	this->y = y;
	this->length = length;

	angle = 0;
}

Shuriken::~Shuriken()
{
}

glm::mat3 Shuriken::getModelMatrix()
{
	return glm::mat3(1) * Transform2D::Translate(x, y) * Transform2D::Rotate(angle);
}

Mesh* Shuriken::getMesh()
{
	return mesh;
}

void Shuriken::update(float deltaTime)
{
	if (is_hit) {
		y -= 500 * deltaTime;
		angle += 3 * deltaTime;
	}
	else {
		x -= 300 * deltaTime;
		angle += 1 * deltaTime;
	}
}

bool Shuriken::oob(WindowObject* window)
{
	if (x < -length) return true;
	if (y < -length) return true;
	return false;
}

float Shuriken::getX()
{
	return x;
}

float Shuriken::getY()
{
	return y;
}

float Shuriken::getLen()
{
	return length;
}

void Shuriken::set_hit(bool is_hit)
{
	if (!this->is_hit) this->is_hit = is_hit;
}

bool Shuriken::getIsHit()
{
	return is_hit;
}
