#include "Arrow.h"
#include <Teme\Tema1\Objects\Transform2D.h>

#define M_PI       3.14159265358979323846

Arrow::Arrow()
{
	is_in_air = false;
}

Arrow::Arrow(Mesh* mesh, float x, float y, float angle, float length)
{
	this->mesh = mesh;
	this->x = x;
	this->y = y;
	this->angle = angle;
	this->length = length;
	
	is_in_air = false;
	speedX = 0;
	speedY = 0;
}

Arrow::~Arrow()
{
}

glm::mat3 Arrow::getModelMatrix()
{
	if (!is_in_air) {
		glm::mat3 modelMatrix = glm::mat3(1)
			* Transform2D::Translate(x , y)
			* Transform2D::Translate(-length, 0) 
			* Transform2D::Rotate(angle) 
			* Transform2D::Translate(length, 0);
		return modelMatrix;
	}
	else {
		glm::mat3 modelMatrix = glm::mat3(1)
			* Transform2D::Translate(x, y)
			* Transform2D::Rotate(angle);
		return modelMatrix;
	}
}

Mesh* Arrow::getMesh()
{
	return mesh;
}

void Arrow::setAngle(float angle)
{
	this->angle = angle;
}

void Arrow::moveY(float movement)
{
	y += movement;
}

void Arrow::fly(float power)
{
	is_in_air = true;
	speedY = power * 1000. * sin(angle);
	speedX = power * 1000. * cos(angle);
	angle = asin(speedY / (sqrt(speedY * speedY + speedX * speedX)));

	y = y + length * sin(angle);
	x = x - length + length * cos(angle);
}

void Arrow::update(float deltaTime)
{
	if (is_in_air) {
		x += speedX * deltaTime;
		y += speedY * deltaTime;
		speedY -= 200. * deltaTime;

		angle = asin(speedY / (sqrt(speedY * speedY + speedX * speedX)));
	}
}

bool Arrow::oob(WindowObject* window)
{
	if (y < -length) return true;
	if (x > window->GetResolution().x + length) return true;
	return false;
}

void Arrow::reset(Bow bow)
{
	x = bow.getX() + length;
	y = bow.getY();
	is_in_air = false;
	speedX = 0;
	speedY = 0;
}

float Arrow::getX()
{
	return x;
}

float Arrow::getY()
{
	return y;
}
