#pragma once
#include <Core\GPU\Mesh.h>
#include <Core\Window\WindowObject.h>
class Shuriken
{
public:
	Shuriken();
	Shuriken(Mesh* mesh, float x, float y, float length);
	~Shuriken();

	glm::mat3 getModelMatrix();
	Mesh* getMesh();
	void update(float deltaTime);
	bool oob(WindowObject* window);
	float getX();
	float getY();
	float getLen();
	void set_hit(bool is_hit);
	bool getIsHit();

private:
	Mesh* mesh;
	float x, y;
	float length;
	float angle;
	bool is_hit;

};

