#include "ObjectsGen.h"

#include <Core/Engine.h>

#define M_PI       3.14159265358979323846

Mesh* ObjectsGen::CreateBalloon(std::string name, float radius, float ratio, glm::vec3 color)
{
    std::vector<VertexFormat> vertices = { VertexFormat(glm::vec3(0,0,0),color) };
    std::vector<unsigned short> indices = { 0 };
    int i = 1;
    float step_phi = 10.;

    for (float phi = 0; phi < 360.; phi += step_phi) {
        vertices.push_back(VertexFormat(radius * glm::vec3(ratio * sin(M_PI * phi / 180), cos(M_PI * phi / 180), 0), color));
        indices.push_back(i++);
    }
    indices.push_back(1);

    Mesh* balloon = new Mesh(name);
    balloon->SetDrawMode(GL_TRIANGLE_FAN);
    balloon->InitFromData(vertices, indices);

    return balloon;
}

Mesh* ObjectsGen::CreateBalloonExplosion(std::string name, float radius1, float radius2, float no_pieces, glm::vec3 color)
{
    std::vector<VertexFormat> vertices = {};
    std::vector<unsigned short> indices = {};

    double phi_step = 360. / no_pieces;

    for (int i = 0; i < no_pieces; i++) {
        vertices.push_back(VertexFormat(radius1 * glm::vec3(
            sin(M_PI * phi_step * i / 180), 
            cos(M_PI * phi_step * i / 180), 0), 
            color));

        vertices.push_back(VertexFormat(radius2 * glm::vec3(
            sin(M_PI * (phi_step * i - phi_step/4) / 180), 
            cos(M_PI * (phi_step * i - phi_step/4) / 180), 0), 
            color));

        vertices.push_back(VertexFormat(radius2 * glm::vec3(
            sin(M_PI * (phi_step * i + phi_step / 4) / 180),
            cos(M_PI * (phi_step * i + phi_step / 4) / 180), 0),
            color));

        indices.push_back(3 * i);
        indices.push_back(3 * i + 1);
        indices.push_back(3 * i + 2);
    }

    Mesh* explosion = new Mesh(name);

    explosion->SetDrawMode(GL_TRIANGLES);
    explosion->InitFromData(vertices, indices);

    return explosion;
}

Mesh* ObjectsGen::CreateBString(std::string name, float len, int points, float points_distance, glm::vec3 color)
{
    std::vector<VertexFormat> vertices = { VertexFormat(glm::vec3(0,0,0),color) };
    std::vector<unsigned short> indices = { 0 };

    float points_len = len / points;

    for (int i = 1; i * points_len < len; i++) {
        if (i % 2 == 1) {
            vertices.push_back(VertexFormat(glm::vec3(-points_distance / 2, -i * points_len, 0), color));
            indices.push_back(i);
        }
        else {
            vertices.push_back(VertexFormat(glm::vec3(points_distance / 2, -i * points_len, 0), color));
            indices.push_back(i);
        }
    }

    vertices.push_back(VertexFormat(glm::vec3(0, -len, 0), color));
    indices.push_back(points);
    
    Mesh* BString = new Mesh(name);
    BString->SetDrawMode(GL_LINE_STRIP);
    BString->InitFromData(vertices, indices);

    return BString;
}

Mesh* ObjectsGen::CreateBow(std::string name, float radius, glm::vec3 color)
{
    std::vector<VertexFormat> vertices = { VertexFormat(glm::vec3(0,0,0),color) };
    std::vector<unsigned short> indices = { 0 };
    int i = 1;
    float step_phi = 10.;

    for (float phi = 0; phi < 181.; phi += step_phi) {
        vertices.push_back(VertexFormat(radius * glm::vec3(sin(M_PI * phi / 180), cos(M_PI * phi / 180), 0), color));
        indices.push_back(i++);
    }

    Mesh* bow = new Mesh(name);
    bow->SetDrawMode(GL_LINE_LOOP);
    bow->InitFromData(vertices, indices);

    return bow;
}

Mesh* ObjectsGen::CreateArrow(std::string name, float length, glm::vec3 color)
{
    std::vector<VertexFormat> vertices =
    {
        VertexFormat(glm::vec3(0,0,0),color),
        VertexFormat(glm::vec3(-length / 10,-length / 20,0),color),
        VertexFormat(glm::vec3(-length / 10,length / 20,0),color),

        VertexFormat(glm::vec3(-length / 10,-length / 50,0),color),
        VertexFormat(glm::vec3(-length / 10,length / 50,0),color),
        VertexFormat(glm::vec3(-length,-length / 50,0),color),
        VertexFormat(glm::vec3(-length,length / 50,0),color)
    };

    std::vector<unsigned short> indices = { 0, 1, 2, 3, 4, 5, 6, 4, 5 };

    Mesh* arrow = new Mesh(name);
    arrow->SetDrawMode(GL_TRIANGLES);
    arrow->InitFromData(vertices, indices);

    return arrow;

}

Mesh* ObjectsGen::CreateRectangle(std::string name, float length, float height, glm::vec3 color)
{
    std::vector<VertexFormat> vertices =
    {
        VertexFormat(glm::vec3(-length / 2,-height / 2,0), color),
        VertexFormat(glm::vec3(-length / 2,height / 2,0), color),
        VertexFormat(glm::vec3(length / 2,-height / 2,0), color),
        VertexFormat(glm::vec3(length / 2,height / 2,0), color)
    };
    std::vector<unsigned short> indices = { 0, 1, 2, 3, 1, 2 };

    Mesh* rectangle = new Mesh(name);
    rectangle->InitFromData(vertices, indices);
    return rectangle;
}

Mesh* ObjectsGen::CreateShuriken(std::string name, float size, glm::vec3 color)
{
    std::vector<VertexFormat> vertices =
    {
        VertexFormat(glm::vec3(0,0,0), color),

        VertexFormat(glm::vec3(size,0,0), color),
        VertexFormat(glm::vec3(size,size,0), color),

        VertexFormat(glm::vec3(0,size,0), color),
        VertexFormat(glm::vec3(-size,size,0), color),

        VertexFormat(glm::vec3(-size,0,0), color),
        VertexFormat(glm::vec3(-size,-size,0), color),

        VertexFormat(glm::vec3(0,-size,0), color),
        VertexFormat(glm::vec3(size,-size,0), color)
    };

    std::vector<unsigned short> indices = { 0,1,2, 0,3,4, 0,5,6, 0,7,8};

    Mesh* shuriken = new Mesh(name);
    shuriken->InitFromData(vertices, indices);
    return shuriken;
}
