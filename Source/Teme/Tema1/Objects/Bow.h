#pragma once
#include <Core\GPU\Mesh.h>

class Bow
{
	public:
		Bow();
		Bow(Mesh* mesh, float x, float y, float angle, float radius);
		~Bow();

		glm::mat3 getModelMatrix();
		Mesh* getMesh();
		void setAngle(float angle);
		void moveY(float movement);
		float getX();
		float getY();
		float getLen();
		float getAngle();
		

	private:
		Mesh* mesh;
		float x, y;
		float angle;
		float radius;

};

