#pragma once
#include <Core\GPU\Mesh.h>
#include <Core\Window\WindowObject.h>

class Balloon
{
	public:
		Balloon();
		Balloon(Mesh * balloonMesh, Mesh* stringMesh, std::vector<Mesh*> explosionSeq, float x, float y, float length, int score_change);
		~Balloon();
	
		glm::mat3 getModelMatrixBalloon();
		glm::mat3 getModelMatrixString();
		Mesh* getMeshBalloon();
		Mesh* getMeshString();
		void update(float deltaTime);
		bool oob(WindowObject* window);
		float getX();
		float getY();
		float getLen();
		void set_hit(bool is_hit);
		int getScore();
		bool getIsHit();

	private:
		Mesh* balloonMesh;
		Mesh* stringMesh;
		std::vector<Mesh*> explosionSeq;
		float x, y;
		float length;
		bool is_hit;
		float time_since_hit;
		int score_change;
};

