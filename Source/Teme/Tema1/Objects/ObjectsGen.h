#pragma once

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

namespace ObjectsGen
{
	Mesh* CreateBalloon(std::string name, float radius, float ratio, glm::vec3 color);

	Mesh* CreateBalloonExplosion(std::string name, float radius1, float radius2, float no_pieces, glm::vec3 color);

	Mesh* CreateBString(std::string name, float len, int points, float points_distance, glm::vec3 color);

	Mesh* CreateBow(std::string name, float radius, glm::vec3 color);
	
	Mesh* CreateArrow(std::string name, float length, glm::vec3 color);

	Mesh* CreateRectangle(std::string name, float length, float height, glm::vec3 color);

	Mesh* CreateShuriken(std::string name, float size, glm::vec3 color);

};

