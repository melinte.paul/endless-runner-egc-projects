#pragma once
#include <Core\GPU\Mesh.h>
#include <Core\Window\WindowObject.h>
#include <Teme\Tema1\Objects\Bow.h>

class Arrow
{
	public:
		Arrow();
		Arrow(Mesh* mesh, float x, float y, float angle, float length);
		~Arrow();

		glm::mat3 getModelMatrix();
		Mesh* getMesh();
		void setAngle(float angle);
		void moveY(float movement);
		void fly(float power);
		void update(float deltaTime);
		bool oob(WindowObject* window);
		void reset(Bow bow);
		float getX();
		float getY();

	private:
		Mesh* mesh;
		float x, y;
		float angle;
		float length;
		bool is_in_air;
		float speedX, speedY;
};

