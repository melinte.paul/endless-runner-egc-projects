#include "Bow.h"
#include <Teme\Tema1\Objects\Transform2D.h>

Bow::Bow()
{
}

Bow::Bow(Mesh* mesh, float x, float y, float angle, float radius)
{
	this->mesh = mesh;
	this->x = x;
	this->y = y;
	this->radius = radius;
	this->angle = angle;
}

Bow::~Bow()
{
}

glm::mat3 Bow::getModelMatrix()
{
	return glm::mat3(1) * Transform2D::Translate(x,y) * Transform2D::Rotate(angle);
}

Mesh* Bow::getMesh()
{
	return mesh;
}

void Bow::setAngle(float angle)
{
	this->angle = angle;
}

void Bow::moveY(float movement)
{
	y += movement;
}

float Bow::getX()
{
	return x;
}

float Bow::getY()
{
	return y;
}

float Bow::getLen()
{
	return radius;
}

float Bow::getAngle()
{
	return angle;
}
