#pragma once

#include <Component/SimpleScene.h>
#include <Core/Engine.h>
#include <Teme\Tema1\Objects\Bow.h>
#include <Teme\Tema1\Objects\Arrow.h>
#include <Teme\Tema1\Objects\Balloon.h>
#include <Teme\Tema1\Objects\Shuriken.h>

class Tema1 : public SimpleScene
{
	public:
		Tema1();
		~Tema1();
	
		void Init() override;

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

		bool balloonArrowCol(Balloon* balloon);
		bool shurikenArrowCol(Shuriken* shuriken);
		bool shurikenBowCol(Shuriken* shuriken);

		int score;
		int lives;

		int mouseX, mouseY;
		bool is_mouse_pressed;

		Bow bow;
		Arrow arrow;
		bool arrow_flying;
		float power;

		float yellowBallonTime, yellowBallonTimeLimit;
		float redBallonTime, redBallonTimeLimit;
		Mesh* balloonString;
		Mesh* balloonRed;
		Mesh* balloonYellow;
		std::vector<Mesh*> explosionSeqRed;
		std::vector<Mesh*> explosionSeqYellow;
		std::vector<Balloon*> balloons;

		Mesh* shuriken;
		float shurikenTime, shurikenTimeLimit;
		std::vector<Shuriken*> shurikens;

		bool alive;
};

