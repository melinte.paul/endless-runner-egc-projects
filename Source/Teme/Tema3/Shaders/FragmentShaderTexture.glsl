#version 330
 
uniform sampler2D texture;
in vec2 texcoord;

layout(location = 0) out vec4 out_color;

void main()
{
	vec4 color = texture2D(texture,texcoord);

	if(color.a < 0.5)
		discard;

	out_color = vec4(color.rgb, 1);

}