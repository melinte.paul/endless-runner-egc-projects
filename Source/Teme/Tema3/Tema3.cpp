#include "Tema3.h"
#include <Component/Transform/Transform.h>
#include <algorithm>
#include <time.h>
#include <iostream>
#include <Teme\Tema1\Objects\Transform2D.h>
#include <Teme\Tema3\Objects\ObjectsGen3.h>

Tema3::Tema3()
{
}

Tema3::~Tema3()
{
}

void Tema3::Init()
{
	srand(time(NULL));

	{
		//Interface camera
		glm::ivec2 resolution = window->GetResolution();
		camera = GetSceneCamera();
		camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
		camera->SetPosition(glm::vec3(0, 0, 50));
		camera->SetRotation(glm::vec3(0, 0, 0));
		camera->Update();
		GetCameraInput()->SetActive(false);
	}

	{
		Shader* shader = new Shader("ShaderInterface");
		shader->AddShader("Source/Teme/Tema3/Shaders/VertexShaderInterface.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema3/Shaders/FragmentShaderInterface.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;

		shader = new Shader("ObjectsShader");
		shader->AddShader("Source/Teme/Tema3/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema3/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;

		shader = new Shader("TextureShader");
		shader->AddShader("Source/Teme/Tema3/Shaders/VertexShaderTexture.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Teme/Tema3/Shaders/FragmentShaderTexture.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;

		shader = new Shader("ShaderLab9");
		shader->AddShader("Source/Laboratoare/Laborator9/Shaders/VertexShader.glsl", GL_VERTEX_SHADER);
		shader->AddShader("Source/Laboratoare/Laborator9/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER);
		shader->CreateAndLink();
		shaders[shader->GetName()] = shader;
	}

	{
		Texture2D* texture = new Texture2D();
		texture->Load2D("Source/Laboratoare/Laborator9/Textures/crate.jpg", GL_REPEAT);
		mapTextures["crate"] = texture;

		texture = new Texture2D();
		texture->Load2D("Source/Teme/Tema3/Textures/Roof.png", GL_REPEAT);
		mapTextures["roof"] = texture;

		texture = new Texture2D();
		texture->Load2D("Source/Teme/Tema3/Textures/Coin.png", GL_REPEAT);
		mapTextures["coin"] = texture;

		texture = new Texture2D();
		texture->Load2D("Source/Teme/Tema3/Textures/Road.png", GL_REPEAT);
		mapTextures["road"] = texture;

		texture = new Texture2D();
		texture->Load2D("Source/Teme/Tema3/Textures/Building.png", GL_REPEAT);
		mapTextures["building"] = texture;
	}

	{
		meshes["sphere"] = ObjectsGen3::CreateSphereMesh(5., glm::vec3(0.5));
		meshes["sphereOutline"] = ObjectsGen3::CreateSphereMesh(5., glm::vec3(0));
		sphere = new Sphere(meshes["sphere"], meshes["sphereOutline"], shaders["ObjectsShader"], 
			glm::vec3(0, 0.5, 0), glm::vec3(1), glm::vec3(0));

		std::vector<glm::vec2> texture = {
			glm::vec2(0,0),
			glm::vec2(0,1),
			glm::vec2(1,0),
			glm::vec2(1,1),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
		};
		meshes["cubeBlue"] = ObjectsGen3::CreateCubeMesh(glm::vec3(0, 0, 0.5),texture);
		meshes["cubePurple"] = ObjectsGen3::CreateCubeMesh(glm::vec3(0.4, 0, 0.6),texture);

		texture = {
			glm::vec2(0,0),
			glm::vec2(0,1),
			glm::vec2(1,1),
			glm::vec2(1,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,1),

			glm::vec2(0,0),
			glm::vec2(0,1),

			glm::vec2(1,1),
			glm::vec2(1,0),

			glm::vec2(1,1),
			glm::vec2(1,0),

			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,1),
			glm::vec2(0,1),

			glm::vec2(1,1),
			glm::vec2(1,1),

			glm::vec2(1,0),
			glm::vec2(1,0)
		};

		meshes["cubeRed"] = ObjectsGen3::CreateCubeMesh(glm::vec3(1, 0, 0),texture);
		
		texture = {
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			glm::vec2(0,0),
			
			glm::vec2(0,1),
			glm::vec2(1,1),

			glm::vec2(0,1),
			glm::vec2(1,1),
			
			glm::vec2(0,0),
			glm::vec2(1,0),

			glm::vec2(0,0),
			glm::vec2(1,0),

			glm::vec2(0,1),
			glm::vec2(0,1),

			glm::vec2(1,1),
			glm::vec2(1,1),

			glm::vec2(0,0),
			glm::vec2(0,0),

			glm::vec2(1,0),
			glm::vec2(1,0)
		};

		meshes["cubeGrey"] = ObjectsGen3::CreateCubeMesh(glm::vec3(0.25),texture);

		meshes["pyramidGrey"] = ObjectsGen3::CreatePyramidMesh(glm::vec3(0.2));

		meshes["fuelRectangleBlack"] = ObjectsGen3::CreateRectangle2D("fuelRectangleBlack", 1, 1, glm::vec3(0));
		meshes["fuelRectangleGreen"] = ObjectsGen3::CreateRectangle2D("fuelRectangleGreen", 1, 1 , glm::vec3(0, 0.8, 0));
		meshes["speedRectangleRed"] = ObjectsGen3::CreateRectangle2D("speedRectangleRed", 1, 1, glm::vec3(0.8, 0, 0));

		meshes["coin"] = ObjectsGen3::CreateRectangle2D("coin", 0.5, 0.5, glm::vec3(1, 1, 0));
	}

	{
		leftPlatformsLength = 0;
		centerPlatformsLength = 6;
		rightPlatformsLength = 0;

		platforms = { new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(-2, -1, -1.5), glm::vec3(5, 1, 3)) };

		while (leftPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(leftPlatformsLength, -1, -6.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			leftPlatformsLength = leftPlatformsLength + newPlatformLength + newSpaceLength;
		}


		while (centerPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(centerPlatformsLength, -1, -1.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			centerPlatformsLength = centerPlatformsLength + newPlatformLength + newSpaceLength;
		}

		while (rightPlatformsLength < 50) {
			int newPlatformLength = rand() % 2 + 3;
			platforms.push_back(new Platform(meshes["cubeBlue"], shaders["VertexColor"],
				glm::vec3(rightPlatformsLength, -1, 3.5), glm::vec3(newPlatformLength, 1, 3)));
			int newSpaceLength = rand() % 10;
			rightPlatformsLength = rightPlatformsLength + newPlatformLength + newSpaceLength;
		}
	}

	{
		leftBuildingsLength = 0;
		rightBuildingsLength = 0;

		while (leftBuildingsLength < 50) {
			buildingsLeft.push_back(leftBuildingsLength);
			int newBuildingSpace = rand() % 4;
			leftBuildingsLength += 1 + newBuildingSpace;
		}
		
		while (rightBuildingsLength < 50) {
			buildingsRight.push_back(rightBuildingsLength);
			int newBuildingSpace = rand() % 4;
			rightBuildingsLength += 1 + newBuildingSpace;
		}
	}

	{
		tp_camera = new EngineComponents::Camera();
		tp_camera->SetPerspective(100, window->props.aspectRatio, 0.01f, 50);
		tp_camera->transform->SetMoveSpeed(0);
		tp_camera->transform->SetWorldPosition(glm::vec3(-3, 2.5f, 0));
		tp_camera->transform->SetWorldRotation(glm::vec3(-25, -90, 0));
		tp_camera->Update();

		fp_camera = new EngineComponents::Camera();
		fp_camera->SetPerspective(100, window->props.aspectRatio, 0.01f, 50);
		fp_camera->transform->SetMoveSpeed(0);
		fp_camera->transform->SetWorldPosition(glm::vec3(0.5, 0.5, 0));
		fp_camera->transform->SetWorldRotation(glm::vec3(0, -90, 0));
		fp_camera->Update();

		active_camera = tp_camera;
	}

	jumping = false;
	alive = true;
	speed = 0;
	fuel = 100;
	score = 0;

	animationTime = 0.f;
}

void Tema3::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0.8, 0.8, 0.8, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();

	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Tema3::Update(float deltaTimeSeconds)
{
	if (deadNoFuelOrRed) {
		timeSincePlatDeath += deltaTimeSeconds;

		glm::vec3 newScale = sphere->getScale() - deltaTimeSeconds * glm::vec3(0.5);
		if (newScale.r < 0) {
			newScale = glm::vec3(0);
		}
		sphere->setScale(newScale);
	}

	if (deadFall) {
		timeSinceFall += deltaTimeSeconds;
		if (timeSinceFall <= 2.f) {
			active_camera = tp_camera;
			tp_camera->transform->SetWorldPosition(glm::vec3(-2, 1, 0) + sphere->getPos());
			tp_camera->transform->SetWorldRotation(glm::vec3(-25, -90, 0));
		}

		glm::vec3 newScale = sphere->getScale() - deltaTimeSeconds * glm::vec3(0.25);
		if (newScale.r < 0) {
			newScale = glm::vec3(0);
		}
		sphere->setScale(newScale);


	}

	{
		fp_camera->transform->SetWorldPosition(glm::vec3(0.5, 0.5, 0) + sphere->getPos());
		if (alive) tp_camera->MoveForward(deltaTimeSeconds);
		active_camera->Update();

		animationTime -= deltaTimeSeconds;
		animationTime = std::max(0.f, animationTime);

		//fuel -= 0.25 * speed * deltaTimeSeconds;
		fuel -= speed * deltaTimeSeconds;
		fuel = std::max(0.f, fuel);
		if (fuel <= 0) {
			alive = false;
			sphere->setSpeedX(0);

			deadNoFuelOrRed = true;
			timeSincePlatDeath = 0.f;
		}

		sphere->Update(deltaTimeSeconds);
		if (sphere->getPos().g == 0.5 && sphere->getSpeedY() == 0) {
			jumping = false;
		}
		if (sphere->getPos().g < -3) {
			alive = false;
			sphere->setSpeedX(0);

			deadFall = true;
			timeSinceFall = 0.f;
		}

		glLineWidth(3);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		RenderMesh(sphere->getOutline(), sphere->getShader(), sphere->getPos(), sphere->getScale(), sphere->getRotation());

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		RenderMesh(sphere->getMesh(), sphere->getShader(), sphere->getPos(), sphere->getScale(), sphere->getRotation());
	}

	{
		float sphereX = sphere->getPos().r;

		for (auto it = platforms.begin(); it != platforms.end(); it++) {
			Platform* platform = *it;
			if (platform->getPos().r + platform->getScale().r + 5 < sphereX) {
				it = platforms.erase(it);
			}
		}

		if (leftPlatformsLength - sphereX < 50 || centerPlatformsLength - sphereX < 50 || rightPlatformsLength - sphereX < 50) {

			Mesh* newPlatforMesh = meshes["cubeBlue"];

			int newCoin = rand() % 2;
			int newObstacle = rand() % 4;

			if (leftPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(leftPlatformsLength, -1, -6.5), glm::vec3(newPlatformLength, 1, 3)));

				if (newCoin == 0) {
					coins.push_back(new Coin(meshes["coin"], shaders["VertexColor"],
						glm::vec3(leftPlatformsLength + newPlatformLength / 2 - 0.5, 0.5, -5), glm::vec3(1)));
				}

				if (newObstacle == 0) {
					obstacles.push_back(new Obstacle(meshes["cubeRed"], shaders["VertexColor"],
						glm::vec3(leftPlatformsLength + newPlatformLength - 2, 0, -5.5), glm::vec3(1)));
				}

				int newSpaceLength = rand() % 10;
				leftPlatformsLength = leftPlatformsLength + newPlatformLength + newSpaceLength;
			}

			if (centerPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(centerPlatformsLength, -1, -1.5), glm::vec3(newPlatformLength, 1, 3)));

				if (newCoin == 0) {
					coins.push_back(new Coin(meshes["coin"], shaders["VertexColor"],
						glm::vec3(centerPlatformsLength + newPlatformLength / 2 - 0.5, 0.5, 0), glm::vec3(1)));
				}

				if (newObstacle == 0) {
					obstacles.push_back(new Obstacle(meshes["cubeRed"], shaders["VertexColor"],
						glm::vec3(centerPlatformsLength + newPlatformLength - 2, 0, -0.5), glm::vec3(1)));
				}

				int newSpaceLength = rand() % 10;
				centerPlatformsLength = centerPlatformsLength + newPlatformLength + newSpaceLength;
			}

			if (rightPlatformsLength - sphereX < 50) {
				int newPlatformLength = rand() % 2 + 3;
				platforms.push_back(new Platform(newPlatforMesh, shaders["VertexColor"],
					glm::vec3(rightPlatformsLength, -1, 3.5), glm::vec3(newPlatformLength, 1, 3)));

				if (newCoin == 0) {
					coins.push_back(new Coin(meshes["coin"], shaders["VertexColor"],
						glm::vec3(rightPlatformsLength + newPlatformLength / 2 - 0.5, 0.5, 5), glm::vec3(1)));
				}

				if (newObstacle == 0) {
					obstacles.push_back(new Obstacle(meshes["cubeRed"], shaders["VertexColor"],
						glm::vec3(rightPlatformsLength + newPlatformLength - 2, 0, 4.5), glm::vec3(1)));
				}

				int newSpaceLength = rand() % 10;
				rightPlatformsLength = rightPlatformsLength + newPlatformLength + newSpaceLength;
			}
		}

		for (auto it = platforms.begin(); it != platforms.end(); it++) {
			Platform* platform = *it;
			if (platform->checkAndHandleColision(sphere)) {
				platform->setMesh(meshes["cubePurple"]);
			}

			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, platform->getPos());
			modelMatrix = glm::scale(modelMatrix, platform->getScale());
			RenderSimpleMesh(platform->getMesh(), shaders["ShaderLab9"], modelMatrix, mapTextures["road"]);

		}

		for (auto it = coins.begin(); it != coins.end(); it++) {
			Coin* coin = *it;
			if (coin->checkAndHandleColision(sphere)) {
				it = coins.erase(it);
				score++;
				printf("Scorul este %d\n", score);
			}
			else {
				coin->Update(deltaTimeSeconds);

				glm::mat4 modelMatrix = glm::mat4(1);
				modelMatrix = glm::translate(modelMatrix, coin->getPos());
				modelMatrix = glm::rotate(modelMatrix, coin->getRot().r, glm::vec3(1, 0, 0));
				modelMatrix = glm::rotate(modelMatrix, coin->getRot().g, glm::vec3(0, 1, 0));
				modelMatrix = glm::rotate(modelMatrix, coin->getRot().b, glm::vec3(0, 0, 1));
				modelMatrix = glm::scale(modelMatrix, coin->getScale());
				RenderSimpleMesh(coin->getMesh(), shaders["ShaderLab9"], modelMatrix, mapTextures["coin"]);
			}
		}

		for (auto it = obstacles.begin(); it != obstacles.end(); it++) {
			Obstacle* obstacle = *it;
			if (obstacle->checkAndHandleColision(sphere)) {
				alive = false;
				sphere->setSpeedX(0);

				deadNoFuelOrRed = true;
				timeSincePlatDeath = 0.f;
			} 
			
			glm::mat4 modelMatrix = glm::mat4(1);			
			modelMatrix = glm::translate(modelMatrix, obstacle->getPos());
			RenderSimpleMesh(obstacle->getMesh(), shaders["ShaderLab9"], modelMatrix, mapTextures["crate"]);
		}

		while (leftBuildingsLength - sphereX < 50) {
			buildingsLeft.push_back(leftBuildingsLength);
			int newBuildingSpace = rand() % 4;
			leftBuildingsLength += 1 + newBuildingSpace;
		}

		while (rightBuildingsLength - sphereX < 50) {
			buildingsRight.push_back(rightBuildingsLength);
			int newBuildingSpace = rand() % 4;
			rightBuildingsLength += 1 + newBuildingSpace;
		}

		for (auto it = buildingsLeft.begin(); it != buildingsLeft.end(); it++) {
			int pos = *it;

			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(pos, -7, -10));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(1, 10, 1));
			RenderSimpleMesh(meshes["cubeGrey"], shaders["ShaderLab9"], modelMatrix, mapTextures["building"]);

			modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(pos, 3, -10));
			RenderSimpleMesh(meshes["pyramidGrey"], shaders["ShaderLab9"], modelMatrix, mapTextures["roof"]);
		}


		for (auto it = buildingsRight.begin(); it != buildingsRight.end(); it++) {
			int pos = *it;

			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(pos, -7, 10));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(1, 10, 1));
			RenderSimpleMesh(meshes["cubeGrey"], shaders["ShaderLab9"], modelMatrix, mapTextures["building"]);

			modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(pos, 3, 10));
			RenderSimpleMesh(meshes["pyramidGrey"], shaders["ShaderLab9"], modelMatrix, mapTextures["roof"]);
		}
	}

	{
	RenderMeshInterface(meshes["speedRectangleRed"], shaders["ShaderInterface"], glm::vec3(400 - 150 * ((5 - speed) / 5), 250, 0), glm::vec3(300 * (speed / 5), 75, 0), glm::vec3(0));
	RenderMeshInterface(meshes["fuelRectangleGreen"], shaders["ShaderInterface"], glm::vec3(400 - 150 * ((100.f - fuel) / 100.f), 150, 0), glm::vec3(300 * (fuel / 100.f), 75, 0), glm::vec3(0));
	RenderMeshInterface(meshes["fuelRectangleBlack"], shaders["ShaderInterface"], glm::vec3(400, 150, 0), glm::vec3(300, 75, 0), glm::vec3(0));
	}
}

void Tema3::RenderSimpleMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix, Texture2D* texture1, Texture2D* texture2)
{
	if (!mesh || !shader || !shader->GetProgramID())
		return;

	// render an object using the specified shader and the specified position
	glUseProgram(shader->program);

	// Bind model matrix
	GLint loc_model_matrix = glGetUniformLocation(shader->program, "Model");
	glUniformMatrix4fv(loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	// Bind view matrix
	glm::mat4 viewMatrix = active_camera->GetViewMatrix();
	int loc_view_matrix = glGetUniformLocation(shader->program, "View");
	glUniformMatrix4fv(loc_view_matrix, 1, GL_FALSE, glm::value_ptr(viewMatrix));

	// Bind projection matrix
	glm::mat4 projectionMatrix = active_camera->GetProjectionMatrix();
	int loc_projection_matrix = glGetUniformLocation(shader->program, "Projection");
	glUniformMatrix4fv(loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(projectionMatrix));

	if (texture1)
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_1"), 0);
	}

	if (texture2)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2->GetTextureID());
		glUniform1i(glGetUniformLocation(shader->program, "texture_2"), 1);
		glUniform1i(glGetUniformLocation(shader->program, "useTexture2"), 1);
	}
	else 	glUniform1i(glGetUniformLocation(shader->program, "useTexture2"), 0);


	// Draw the object
	glBindVertexArray(mesh->GetBuffers()->VAO);
	glDrawElements(mesh->GetDrawMode(), static_cast<int>(mesh->indices.size()), GL_UNSIGNED_SHORT, 0);
}

void Tema3::RenderMeshInterface(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(camera->GetProjectionMatrix()));

	glm::mat4 model(1);
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	model = glm::rotate(model, rotation.r, glm::vec3(1, 0, 0));
	model = glm::rotate(model, rotation.g, glm::vec3(0, 1, 0));
	model = glm::rotate(model, rotation.b, glm::vec3(0, 0, 1));

	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(model));
	mesh->Render();
}

void Tema3::RenderMesh(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetProjectionMatrix()));

	int timeLocation = glGetUniformLocation(shader->program, "time");
	glUniform1f(timeLocation, animationTime);

	glm::mat4 model(1);
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	model = glm::rotate(model, rotation.r, glm::vec3(1, 0, 0));
	model = glm::rotate(model, rotation.g, glm::vec3(0, 1, 0));
	model = glm::rotate(model, rotation.b, glm::vec3(0, 0, 1));

	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(model));
	mesh->Render();
}

void Tema3::RenderMeshTextured(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Texture2D* texture)
{
	if (!mesh || !shader || !shader->program)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, GL_FALSE, glm::value_ptr(active_camera->GetProjectionMatrix()));

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->GetTextureID());
	glUniform1i(glGetUniformLocation(shader->program, "texture"), 0);

	int timeLocation = glGetUniformLocation(shader->program, "time");
	glUniform1f(timeLocation, animationTime);

	glm::mat4 model(1);
	model = glm::translate(model, position);
	model = glm::scale(model, scale);
	model = glm::rotate(model, rotation.r, glm::vec3(1, 0, 0));
	model = glm::rotate(model, rotation.g, glm::vec3(0, 1, 0));
	model = glm::rotate(model, rotation.b, glm::vec3(0, 0, 1));

	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(model));
	mesh->Render();
}


void Tema3::FrameEnd()
{
	//DrawCoordinatSystem(active_camera->GetViewMatrix(), active_camera->GetProjectionMatrix());
}


void Tema3::OnInputUpdate(float deltaTime, int mods)
{
	{
		if (window->KeyHold(GLFW_KEY_A) && alive) {
			sphere->moveLeft(deltaTime);
		}

		if (window->KeyHold(GLFW_KEY_D) && alive) {
			sphere->moveRight(deltaTime);
		}
	}

}

void Tema3::OnKeyPress(int key, int mods)
{
	{
		if (key == GLFW_KEY_W && alive) {
			speed += 1;
		}
		if (key == GLFW_KEY_S && alive) {
			speed -= 1;
		}

		speed = std::min(speed, 5.f);
		speed = std::max(1.f, speed);

		tp_camera->transform->SetMoveSpeed(speed * 2);
		sphere->setSpeedX(speed * 2);
	}

	{
		if (key == GLFW_KEY_SPACE && jumping == false && alive) {
			sphere->setSpeedY(2);
			jumping = true;
		}
	}

	{
		if (key == GLFW_KEY_E && alive) {
			if (active_camera == tp_camera) {
				active_camera = fp_camera;
			}
			else active_camera = tp_camera;
		}
	}
}

void Tema3::OnKeyRelease(int key, int mods)
{
}

void Tema3::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
}

void Tema3::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
}

void Tema3::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
}

void Tema3::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Tema3::OnWindowResize(int width, int height)
{
}
