#pragma once
#include <Component\SimpleScene.h>
#include <Teme\Tema2\Objects\Sphere.h>
class Coin
{
public:
	Coin();
	Coin(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale);
	~Coin();

	Mesh* getMesh();
	Shader* getShader();
	glm::vec3 getPos();
	glm::vec3 getScale();
	glm::vec3 getRot();

	void Update(double deltaTime);

	bool checkAndHandleColision(Sphere* sphere);

private:
	Mesh* mesh;
	Shader* shader;
	glm::vec3 pos;
	glm::vec3 scale;
	glm::vec3 rot;

};

