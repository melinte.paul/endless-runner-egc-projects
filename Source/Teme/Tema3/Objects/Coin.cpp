#include "Coin.h"

Coin::Coin()
{
}

Coin::Coin(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale)
{
	this->mesh = mesh;
	this->shader = shader;
	this->pos = pos;
	this->scale = scale;
	rot = glm::vec3(0, 1.57, 0);
}

Coin::~Coin()
{
}

Mesh* Coin::getMesh()
{
	return mesh;
}

Shader* Coin::getShader()
{
	return shader;
}

glm::vec3 Coin::getPos()
{
	return pos;
}

glm::vec3 Coin::getScale()
{
	return scale;
}

glm::vec3 Coin::getRot()
{
	return rot;
}

void Coin::Update(double deltaTime)
{
	rot += glm::vec3(0, deltaTime, 0);
}

bool Coin::checkAndHandleColision(Sphere* sphere)
{
	if (glm::distance(pos, sphere->getPos()) < 1.) {
		return true;
	}
	return false;
}
