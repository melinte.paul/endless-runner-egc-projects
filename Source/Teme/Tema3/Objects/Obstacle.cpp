#include "Obstacle.h"

Obstacle::Obstacle()
{
}

Obstacle::Obstacle(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale)
{
	this->mesh = mesh;
	this->shader = shader;
	this->pos = pos;
	this->scale = scale;
}

Obstacle::~Obstacle()
{
}

Mesh* Obstacle::getMesh()
{
	return mesh;
}

Shader* Obstacle::getShader()
{
	return shader;
}

glm::vec3 Obstacle::getPos()
{
	return pos;
}

glm::vec3 Obstacle::getScale()
{
	return scale;
}

bool Obstacle::checkAndHandleColision(Sphere* sphere)
{
	if (glm::distance(pos, sphere->getPos()) < 1.) {
		return true;
	}
	return false;
}
