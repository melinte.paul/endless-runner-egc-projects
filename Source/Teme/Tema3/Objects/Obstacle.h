#pragma once
#include <Component\SimpleScene.h>
#include <Teme\Tema2\Objects\Sphere.h>

class Obstacle
{
public:
	Obstacle();
	Obstacle(Mesh* mesh, Shader* shader, glm::vec3 pos, glm::vec3 scale);
	~Obstacle();

	Mesh* getMesh();
	Shader* getShader();
	glm::vec3 getPos();
	glm::vec3 getScale();

	bool checkAndHandleColision(Sphere* sphere);

private:
	Mesh* mesh;
	Shader* shader;
	glm::vec3 pos;
	glm::vec3 scale;
};

