#pragma once

#include <Component/SimpleScene.h>
#include <Teme\Tema2\Objects\Sphere.h>
#include <Teme\Tema2\Objects\Platform.h>
#include <Teme\Tema3\Objects\Coin.h>
#include <Teme\Tema3\Objects\Obstacle.h>
#include <Core/GPU/Mesh.h>

class Tema3 : public SimpleScene
{
	public:
		Tema3();
		~Tema3();

		void Init() override;
		void RenderMesh(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation);
		void RenderMeshTextured(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation, Texture2D* texture);
		void RenderMeshInterface(Mesh* mesh, Shader* shader, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation);
		void RenderSimpleMesh(Mesh* mesh, Shader* shader, const glm::mat4& modelMatrix, Texture2D* texture1 = NULL, Texture2D* texture2 = NULL);

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

	protected:
		EngineComponents::Camera *active_camera;
		EngineComponents::Camera *fp_camera;
		EngineComponents::Camera *tp_camera;
		EngineComponents::Camera* camera;

		Sphere* sphere;
		float speed;
		bool jumping;
		bool alive;
		float fuel;
		
		bool deadNoFuelOrRed;
		bool deadFall;

		float timeSinceFall;
		float timeSincePlatDeath;

		float animationTime;

		std::vector<Platform*> platforms;
		int leftPlatformsLength, centerPlatformsLength, rightPlatformsLength;

		std::vector<Coin*> coins;
		int score;

		std::vector<Obstacle*> obstacles;

		std::vector<int> buildingsLeft;
		std::vector<int> buildingsRight;
		int leftBuildingsLength, rightBuildingsLength;

		std::unordered_map<std::string, Texture2D*> mapTextures;
};